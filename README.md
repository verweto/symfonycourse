# Symfony Basics Course

## Software

- Vagrant: https://www.vagrantup.com/downloads.html
- Virtualbox: https://www.virtualbox.org/wiki/Downloads
- Phpstorm: https://www.jetbrains.com/phpstorm/download/


## Installation

The source files of the course are located in GIT. Please make sure to clone the repository to your computer:

```
git clone https://bitbucket.org/verweto/symfonycourse.git
```

Because every operating system works in a slightly different way, a Vagrantfile is added to the repository. This file will make it easy for setting up your development environment.
Setting up the development environment will take a while.

### Unix / Mac
```
git fetch && git checkout ST0-setup-unix
vagrant up
sudo sh -c 'echo 192.168.56.101 symfonycourse.local >> /etc/hosts'
```

### Windows
```
git fetch && git checkout ST0-setup-windows
vagrant up
```

When vagrant ran, you have to open up notepad as an Administrator and add:

192.168.56.101 symfonycourse.local


to the file:


C:\Windows\System32\Drivers\Etc\hosts


## Done!

When all commands ran, you should see an welcome page when you surf to http://symfonycourse.local/
